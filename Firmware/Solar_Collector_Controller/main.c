/*
 * main.c
 *
 *  Created on: 20 05 2016
 *      Author: Adam Stasiak
 */


#include <avr/io.h>

#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include <avr/wdt.h>

#include "LCD/lcd44780.h"

#define KEY1 (1<<PC3)
#define KEY2 (1<<PC4)
#define KEY3 (1<<PC5)

#define ADCIN (1<<PC0)   


#define ALARM (1<<PB1)
#define ALARM_ON PORTB |= ALARM
#define ALARM_OFF PORTB &= ~ALARM

#define PUMP (1<<PB0)
#define PUMP_ON PORTB |= PUMP
#define PUMP_OFF PORTB &= ~PUMP

#define PUMP_LED (1<<PD7)
#define PUMP_LED_ON PORTD &= ~PUMP_LED
#define PUMP_LED_OFF PORTD |= PUMP_LED

uint8_t znak_stopnie[] = {4,10,4,32,32,32,32,32}; /* znak stopni */
uint8_t znak_o[] = {4,32,14,17,17,17,14,32};
uint8_t znak_z[] = {4,32,31,2,4,8,31,32};
	
uint8_t KEY_PRESS(uint8_t which);
uint16_t measurement(uint8_t pin);
void temp_display(void);
void Ustawienia(void);
void odczyt_temp(void);
void Alarm(void); 
void Histereza1(void); 
void Histereza2(void);
void Kalibracja(void);
void Czujnik1(void);
void Czujnik2(void);
void ON_OFF(void);


uint8_t TS=0;

uint8_t TB=0;

volatile uint8_t sec;
uint8_t x=1;
uint8_t a=0;
uint8_t z=0;			

uint8_t h1=1;        
uint8_t EEMEM h1_1;

uint8_t tr=1;
uint8_t EEMEM tr_1;

uint8_t al=1;		
uint8_t EEMEM al_1;

int8_t c1=0;
int8_t EEMEM c1_1;

int8_t c2=0;
int8_t EEMEM c2_1;

int main(){


	DDRC &= ~(KEY1 | KEY2 | KEY3);
	PORTC |= (KEY1 | KEY2 | KEY3);

	DDRB |= (ALARM | PUMP );
	PORTB &= ~(ALARM | PUMP);
	          

	DDRD |= PUMP_LED;
	PORTD |= PUMP_LED;
	

	lcd_init(); //display initializes//
	
	TCCR2 |= (1<<WGM21);				        
	TCCR2 |= (1<<CS20)|(1<<CS21)|(1<<CS21);		 
	OCR2 = 77;							         
	TIMSK |= (1<<OCIE2);

	ADMUX |= (1<<REFS0)|(1<<REFS1);
	ADCSRA |= (1<<ADEN)|(1<<ADPS1)|(1<<ADPS2);

	sei(); //global authorization to interrupt//

	//Startup screen//
	lcd_locate(0,0);
	lcd_str("Solar Controller");
	lcd_locate(1,0);
	lcd_str("by Adam Stasiak");
	_delay_ms(5000);
	lcd_cls(); //erasing the contents of the display//
	
	lcd_defchar(0x81, znak_stopnie);
	lcd_defchar(0x82, znak_o);
	lcd_defchar(0x83, znak_z);
	
	h1=eeprom_read_byte(&h1_1); /* odczyt histerezy1 z EEPROM-u */
    tr=eeprom_read_byte(&tr_1); /* odczyt histerezy2 z EEPROM-u */
	al=eeprom_read_byte(&al_1); /* odczyt alarmu przegrzania z EEPROM-u */
	c1=eeprom_read_byte(&c1_1);
	c2=eeprom_read_byte(&c2_1);
	
	
	while(1){
		
		wdt_enable( WDTO_1S ); /* start watchdog'a */
		
		if(sec>=5){		
			odczyt_temp();
			sec=0;			
		}
			
		temp_display();
		
		ON_OFF();					
		
		if(KEY_PRESS(KEY3)){
			a=0;
			sec=0;
			z=0;
			Ustawienia();	
		}
				
		wdt_reset();
		
	}
}

void Ustawienia(void){
	
	wdt_disable(); 
	lcd_cls();
	if(a==0){
		lcd_locate(0,3);
		lcd_str("Ustawienia");
		_delay_ms(1500);
		lcd_cls();	
	}
	x=1;
	
	while(1){
		
		ON_OFF();
		
		if(sec>120){
			lcd_cls();
			z=1;
			break;
		}
		
		
		if(x>5)x=1;
		if(x<1)x=5;
		
		if(KEY_PRESS(KEY2))x++;
		if(KEY_PRESS(KEY1))x--;
		
			if(x==1){

				lcd_locate(0,0);
				lcd_str("  >Histereza1<  ");
				lcd_locate(1,0); 
				lcd_str("   T. r""\x82""\x83""nicy   ");

				if(KEY_PRESS(KEY3)){
					lcd_cls();
					Histereza1();
				}
			}
			if(x==2){

				lcd_locate(0,0);
				lcd_str("  >T. r""\x82""\x83""nicy<  ");
				lcd_locate(1,0);
				lcd_str("   Alarm prz.   ");

				if(KEY_PRESS(KEY3)){
					lcd_cls();
					Histereza2();
				}					
			}
			if(x==3){

				lcd_locate(0,0);
				lcd_str("  >Alarm prz.<  ");
				lcd_locate(1,0);
				lcd_str(" Kalibracja.czu ");

				if(KEY_PRESS(KEY3)){
					lcd_cls();
					Alarm();
				}
			}
			if(x==4){

				lcd_locate(0,0);
				lcd_str(">Kalibracja.czu<");
				lcd_locate(1,0);
				lcd_str("      Exit      ");

				if(KEY_PRESS(KEY3)){
					lcd_cls();
					Kalibracja();
				}
			}
			if(x==5){

				lcd_locate(0,0);
				lcd_str("     >Exit<     ");
				lcd_locate(1,0);
				lcd_str("   Histereza1   ");

				if(KEY_PRESS(KEY3)){
					lcd_cls();
					a=0;
					z=1;
					break;
				}				
			}
			
			if(z==1){
				lcd_cls();
				break;	
			}
			
			odczyt_temp();

	}
	
			
}

void Histereza1(void){
	
		while(1){
			
		
			odczyt_temp();
			ON_OFF();
			
		if(sec>60){
			lcd_cls();
			z=1;
			break;
		}
		
			if(z==1){
				lcd_cls();
				break;
			}			
			lcd_locate(0,3);
			lcd_str("Histereza1");
			lcd_locate(1,7);
			lcd_int(h1);
			lcd_str("\x81""C");

			if(KEY_PRESS(KEY1)){ /* po wcisnieciu KEY1 zwi�ksz histereze1 */
				h1++;
			}

			if(KEY_PRESS(KEY2)){ /* po wcisnieciu KEY2 zmniejsz histereze1 */
				h1--;
			}

			/* zakrez histerezy1 */
			if(h1>2)h1=1;
			if(h1==0)h1=2;

			if(KEY_PRESS(KEY3)){ /* po wcisni�ciu KEY3 wr�� do ustawie� serwisowych */
				lcd_cls();
				eeprom_write_byte (&h1_1, h1); /* zapis histerezy1 do EEPROM-u */
				a=1;
				Ustawienia();
			}
	
		}
}

void Histereza2(void){
	
	while(1){
		
		odczyt_temp();
		ON_OFF();
		
		if(sec>60){
			lcd_cls();
			z=1;
			break;
		}
		
			if(z==1){
				lcd_cls();
				break;
			}

		lcd_locate(0,3);
		lcd_str("T. r""\x82""\x83""nicy");
		if(tr<10){
			lcd_locate(1,6);
			lcd_str(" ");
			lcd_int(tr);
			lcd_str("\x81""C ");
		}else{
     		lcd_locate(1,6);
			lcd_int(tr);
			lcd_str("\x81""C");
			
		}
		


		if(KEY_PRESS(KEY1)){ /* po wcisnieciu KEY1 zwi�ksz histereze1 */
			tr++;
		}

		if(KEY_PRESS(KEY2)){ /* po wcisnieciu KEY2 zmniejsz histereze1 */
			tr--;
		}

		/* zakrez histerezy1 */
		if(tr>20)tr=5;
		if(tr<5)tr=20;

		if(KEY_PRESS(KEY3)){ /* po wcisni�ciu KEY3 wr�� do ustawie� serwisowych */
			lcd_cls();
			eeprom_write_byte (&tr_1, tr); /* zapis histerezy1 do EEPROM-u */
			a=1;
			Ustawienia();
		}
		
	}
}

void Alarm(void){
	
	while(1){
		
		odczyt_temp();
		ON_OFF();
		
		if(sec>60){
			lcd_cls();
			z=1;
			break;
		}
				
		if(z==1){
			lcd_cls();
			break;
		}

		lcd_locate(0,1);
		lcd_str("Alarm przegrz.");
		lcd_locate(1,6);
		lcd_int(al);
		lcd_str("\x81""C");

		if(KEY_PRESS(KEY1)){ /* po wcisnieciu KEY1 zwi�ksz temperature alarmu */
			al++;
		}
		if(KEY_PRESS(KEY2)){ /* po wcisnieciu KEY2 zmniejsz temperature alarmu */
			al--;
		}

		/* zakrez temperatury alarmu */
		if(al>90)al=60;
		if(al<60)al=90;

		if(KEY_PRESS(KEY3)){ /* po wcisni�ciu KEY3 wr�� do ustawie� serwisowych */
			lcd_cls();
			eeprom_write_byte (&al_1, al); /* zapis temperatury przegrzania do EEPROM-u */
			a=1;
			Ustawienia();
		}
	
	}
	
}

void Kalibracja(void){
	
	while(1){
		
		odczyt_temp();
		ON_OFF();
		
		if(sec>60){
			lcd_cls();
			z=1;
			break;
		}
		
		if(z==1){
			lcd_cls();
			break;
		}
			
		if(x>3)x=1;
		if(x<1)x=3;
		
		if(KEY_PRESS(KEY2))x++;
		if(KEY_PRESS(KEY1))x--;
		
		if(x==1){

			lcd_locate(0,0);
			lcd_str("  >Czujnik_TS< ");
			lcd_locate(1,0);
			lcd_str("   Czujnik_TB   ");

			if(KEY_PRESS(KEY3)){
				lcd_cls();
				Czujnik1();
			}
		}
		if(x==2){

			lcd_locate(0,0);
			lcd_str("  >Czujnik_TB<  ");
			lcd_locate(1,0);
			lcd_str("      Exit      ");

			if(KEY_PRESS(KEY3)){
				lcd_cls();
				Czujnik2();
			}
		}
		if(x==3){

			lcd_locate(0,0);
			lcd_str("     >Exit<     ");
			lcd_locate(1,0);
			lcd_str("   Czujnik_TS   ");

			if(KEY_PRESS(KEY3)){
				lcd_cls();
				a=1;
				lcd_cls();
				Ustawienia();
			}
		}
	
	}
}

void Czujnik1(void){
	
		while(1){
			
			odczyt_temp();
			ON_OFF();
			
			if(sec>60){
				lcd_cls();
				z=1;
				break;
			}
			
			if(z==1){
				lcd_cls();
				break;
			}

			if(c1<0){
				lcd_locate(0,0);
				lcd_str("   Czujnik_TS   ");
				lcd_locate(1,6);
				lcd_int(c1);
				lcd_str("\x81""C");				
			}
			else{
				lcd_locate(0,0);
				lcd_str("   Czujnik_TS   ");
				lcd_locate(1,6);
				lcd_str(" ");
				lcd_int(c1);
				lcd_str("\x81""C");
			}


			if(KEY_PRESS(KEY1)){ /* po wcisnieciu KEY1 zwi�ksz histereze1 */
				c1++;
			}

			if(KEY_PRESS(KEY2)){ /* po wcisnieciu KEY2 zmniejsz histereze1 */
				c1--;
			}

	
			if(c1>5)c1=-5;
			if(c1<-5)c1=5;

			if(KEY_PRESS(KEY3)){ /* po wcisni�ciu KEY3 wr�� do ustawie� serwisowych */
				lcd_cls();
				eeprom_write_byte (&c1_1, c1);
				a=1;
				Kalibracja();
			}
			
		}
	
}

void Czujnik2(void){
	
	while(1){
		
		odczyt_temp();
		ON_OFF();
		
		if(sec>60){
			lcd_cls();
			z=1;
			break;
		}
		
		if(z==1){
			lcd_cls();
			break;
		}

		if(c2<0){
			lcd_locate(0,0);
			lcd_str("   Czujnik_TB   ");
			lcd_locate(1,6);
			lcd_int(c2);
			lcd_str("\x81""C");
		}
		else{
			lcd_locate(0,0);
			lcd_str("   Czujnik_TB   ");
			lcd_locate(1,6);
			lcd_str(" ");
			lcd_int(c2);
			lcd_str("\x81""C");
		}


		if(KEY_PRESS(KEY1)){ /* po wcisnieciu KEY1 zwi�ksz histereze1 */
			c2++;
		}

		if(KEY_PRESS(KEY2)){ /* po wcisnieciu KEY2 zmniejsz histereze1 */
			c2--;
		}

		
		if(c2>5)c2=-5;
		if(c2<-5)c2=5;

		if(KEY_PRESS(KEY3)){ /* po wcisni�ciu KEY3 wr�� do ustawie� serwisowych */
			lcd_cls();
			eeprom_write_byte (&c2_1, c2);
			a=1;
			Kalibracja();
		}
	
	}
	
}

void temp_display(void){
	
			lcd_locate(0,0);
			lcd_str("Solar :");
			
			lcd_locate(1,0);
			lcd_str("Boiler:");
			
			lcd_locate(0,7);
			lcd_int(TS);
			lcd_str("\x81""C");
			
			
			lcd_locate(1,7);
			lcd_int(TB);
			lcd_str("\x81""C");
			
}

void ON_OFF(void){
	
		if((TS-tr)>=(TB+h1)){
			PUMP_ON;
			PUMP_LED_ON;
		}

		if((TB-h1)>=(TS-tr)){
			PUMP_OFF;
			PUMP_LED_OFF;
		}
		
		if(((TB)||(TS))>al){
			PUMP_ON;
			PUMP_LED_ON;		
			ALARM_ON;		
		}
		else{
			ALARM_OFF;
		}
		
		if((TS)<2)ALARM_ON;
		else ALARM_OFF;
		
}

uint16_t measurement(uint8_t pin){

	ADMUX = (ADMUX & 0xF8) | pin;
	ADMUX |= pin;
	ADCSRA |= (1<<ADSC);
	while(ADCSRA & (1<<ADSC));

	return ADC;
}

uint8_t KEY_PRESS(uint8_t which)
{
	if(!(PINC & which)){
		_delay_ms(100);
	}
		if(!(PINC & which)){
			while(!(PINC & which));
			return 1;
		}
	return 0;
}

void odczyt_temp(void){
	
		measurement(1);
		TS=((ADC*2.56/1024)*100)+c1;
		
					
		measurement(0);
		TB=((ADC*2.56/1024)*100)+c2;
		
					
}

ISR(TIMER2_COMP_vect)
{
	static uint16_t cnt=0;	
	
	 if(cnt>=3000)
		{					
				sec++;	
				cnt=0;
		}

	cnt++;
	
}


